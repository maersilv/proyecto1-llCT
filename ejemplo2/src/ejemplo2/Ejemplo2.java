/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo2;

import javax.swing.JOptionPane;

/**
 *
 * @author estudiante
 */
public class Ejemplo2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        //operad0res aritmericos
//        //con enteros
//        int n=6;
//        System.out.println(7+n);
//         //con doole
//         double x=5.2;
//         System.out.println(6*x);
//         //asignacion
//         int c=2;
//         System.out.println(c*=15);
//         //leer el string y luego converlirlo en lo que queremos
//         //+tipos de imperion 3 por el momento
//        //ln=hace el print
//        System.out.println("hi");
//        //no hace el salto de linea
//        System.out.print("hello");
//        //a ocurrido un error
//        System.err.println("error");
//        //joptionpan=para leer
//        String name= JOptionPane.showInputDialog("Digite su nombre:");
//        System.out.println(name);
//        //forma de mostrar datos
//        JOptionPane.showMessageDialog(null
//                ,"texto o salida",
//                "titulo",JOptionPane.INFORMATION_MESSAGE);
//        //pregunta
//         JOptionPane.showMessageDialog(null
//                ,"texto o salida",
//                "titulo",JOptionPane.QUESTION_MESSAGE);
//          JOptionPane.showMessageDialog(null,"Error!","ERROR",
//                  JOptionPane.ERROR_MESSAGE);
//          JOptionPane.showMessageDialog(null,"Cuidado!",
//                  "AdVERTENCIA",JOptionPane.WARNING_MESSAGE);
//          JOptionPane.showMessageDialog(null,"sin icono",
//                  "Mensaje",JOptionPane.PLAIN_MESSAGE);
//          
//          //ejemplo
//          int annoActual=2017;
//          int mesActual=5;
//          int diaActual=19;
//          
//          String anno=JOptionPane.showInputDialog("Digite su año de nacimiento:");
//          String mes=JOptionPane.showInputDialog("Digite su mes de nacimiento:");
//          String dia=JOptionPane.showInputDialog("Digite su dia de nacimiento:");
//            int annoUsuario=Integer.parseInt(anno);
//            int mesU=Integer.parseInt(mes);
//            int diaU=Integer.parseInt(dia);
//            int annoUs=annoActual - annoUsuario;
//            int mes1=mesActual-mesU;
//            int dia1=diaActual-diaU;
//           JOptionPane.showMessageDialog(null,("su edad es:"+annoUs+"años"+mes1+"meses"+dia1+"dias"),
//                   "SU EDAD ES:"
//            ,JOptionPane.INFORMATION_MESSAGE);
//           //!=representa diferente
//           //a=7 es permitido // 7=a ,no es permitido 
//           //== la igualdad es conmutativa
//          System.out.println("5==5:"+(5==5));
//          System.out.println("5==6:"+(5==6));
//          System.out.println("5!=6:"+(5!=6));
//          System.out.println("5>6:"+(5>6));
//          System.out.println("5<=6:"+(5<=6));
//          System.out.println("6<=6:"+(6<=6));
//          if (5==5){
//              System.out.println("es verdadero");
//              
//          }
//          else{
//              if (5>6){
//                  System.out.println("es falso");
//                  
//              }
//          }
//              
        //and true +true =true
        //or false +false=false
        //not true false ,false true
        //aplicacion del and&&
        boolean z = true;
        boolean y = false;
        System.out.println(z && y);
        z = true;
        y = true;
        System.out.println(z && y);
        //estructuras de condicion
        //if(condicion){
        //bloque de codigo
        //}
        //condicion simple
        int edad = 18;
        boolean res=edad>=18;
        System.out.println("Estructuras de control");
        if (res) {
            System.out.println("tiene permiso");
        }
        //condicion compleja
        int edad1 = 17;
        boolean res2=edad>=18;
        System.out.println("Estructuras de control");
        if (res2) {
            System.out.println("tiene permiso");
        }
        else{
            System.out.println("No tiene permiso");
            
        }
        //par x%2==0
        int n=Integer.parseInt(JOptionPane.showInputDialog("Digite su numero:"));
        if(n%2==0){
            System.out.println("variable n= "+n+"es par");
        }
        else{
            System.out.println("variable n= "+n+"es impar");
            
        }

    }

}
//alt+shif+f para acomodar el codigo