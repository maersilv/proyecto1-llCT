/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semana7;

/**
 *
 * @author estudiante
 */
public class Ciudad {
    private String nombre;
    private int tarifa;

    public Ciudad(String nombre, int tarifa) {
        this.nombre = nombre;
        this.tarifa = tarifa;
    }

    public String getNombre() {
        return nombre;
    }

    public int getTarifa() {
        return tarifa;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTarifa(int tarifa) {
        this.tarifa = tarifa;
    }
    
}
