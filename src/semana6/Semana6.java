/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semana6;

/**
 *
 * @author estudiante
 */
public class Semana6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //dimension =tamaño
        //int arreglo[]=new int[9];
        //el arreglo es statico,no se expande
        //numerito adentro[2]se llama subindice
        //c.length devuelve el largo
        int arreglo[]=new int[5];//declarado e inicializado
        //siempre inicia en 0
        arreglo[2]=10;
        arreglo[4]=6;
        //imprimir en el ultimo
        arreglo[arreglo.length-1]=8;
        System.out.println(arreglo.length);
        System.out.println(arreglo[0]);
        System.out.println(arreglo[1]);
        System.out.println(arreglo[2]);
        System.out.println(arreglo[3]);
        System.out.println(arreglo[4]);
        
        //si son mas 
        System.out.println("como imprimir un arreglo");
        for (int i=0; i<arreglo.length; i++){
            System.out.print(arreglo[i]+" ");//sinln en print para impirmir en linea
            //y con ln para abajo
        }
        System.out.println("");
        
        
        //arreglo.length
                //cualquier tipo de dato
                //plural casi todos los nombres
                float monedas[]=new float[100];
                String dias[]={"lunes","martes","miercoles","jueves","viernes",
                "sabado","domingo"};
                int digitos[]= {1,2,3,4,5,6,7};
                String nombres[];
                nombres=new String[3];
                nombres[0]=new String("ana");
                nombres[1]=new String("tris");
                System.out.println(nombres[0]);
                System.out.println(nombres[1]);
    
                 
                Persona p=new Persona(207790373,"maria","silva");
              Persona[] personas=new Persona[10];
                System.out.println(p);
                
        for (int i=0; i<personas.length; i++){
            System.out.print(personas[i]+" ");//sinln en print para impirmir en linea
            //y con ln para abajo
        }
        System.out.println("");
                
        
    }
    
    }
    

